<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign up Form</h3>
    <form action="/selamatdatang" method="POST">
    @csrf
    <label>First Name: </label><br><br>
    <input type="text" name="nama1"><br><br>
    <label>Last Name: </label><br><br>
    <input type="text" name="nama2"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gen">Male<br>
    <input type="radio" name="gen">Female<br>
    <input type="radio" name="gen">Other<br>
    <br><br>
    <label>Nationality</label><br><br>
    <select>
        <option value="1">Indonesian</option>
        <option value="2">Malaysian</option>
        <option value="3">Singapore</option>
        <option value="4">Other</option>
    </select>
    <br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Other<br>
    <br><br>
    <label>Bio:</label><br><br>
    <textarea name="alamat" cols="30" rows="10">Bio:</textarea>
    <br>
    <input type="submit">
    </form>

</body>
</html>